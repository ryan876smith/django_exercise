from django.core.exceptions import ValidationError
from django.db import models
import re

# Create your models here.

def is_mac_address(value):
    if not re.match('([0-9a-fA-F]{12}',value):
        raise ValidationError(
            _('%(value)s is not a valid mac address'),
            params={'value': value},
        )


class MonitoredInterface(models.Model):
    external_id = models.CharField(max_length=40,)
    mac_address = models.CharField(max_length=12,validators=[is_mac_address])
    ipv4_address = models.CharField(max_length=12)
    interface_name = models.CharField(max_length=12)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)   

    class Meta:
        unique_together = ('mac_address', 'interface_name',)