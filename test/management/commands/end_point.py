from django.core.management.base import BaseCommand, CommandError

# import additional classes/modules as needed
from test.models import MonitoredInterface

import requests , os  , json

class Command(BaseCommand):
    help = 'Insert or Update into Monitoring'


    def handle(self, *args, **options):

        counter = 0
        data    = [ os.getenv('BASE_URL_ALPHA')  , os.getenv('BASE_URL_BRAVO') ]

        for url in data:
            
                request = requests.get(url)

                for item in request.json() :
                    
                    inserts = []   
                    
                    if "_id" in item :
                        record = { 'external_id' : item["_id"] , 'mac_address' : item["mac"].replace(":" ,"")  , 'ipv4_address' : item["address"] ,'interface_name' : item["interface"] }
                        inserts.append(record)
                    elif "id" in item : 

                        for interface in item["interfaces"]:
                            record = { 'external_id' : interface["id"] , 'mac_address' : item["mac_address"] , 'ipv4_address' : interface["ipv4"] ,'interface_name' : interface["name"] }  
                        inserts.append(record)

                    for row in inserts:
                        
                        print(row['external_id'])
                        obj, created = MonitoredInterface.objects.update_or_create( mac_address= row['mac_address'] , interface_name= row['interface_name'] , defaults=row  )   

                        if not created :
                            counter = counter + 1
                        
                        print(obj.id) 
                        print(created)         
               